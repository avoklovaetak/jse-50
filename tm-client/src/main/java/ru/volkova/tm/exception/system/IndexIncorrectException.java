package ru.volkova.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException(@NotNull String value) {
        super("Error! This value ``" + value + "`` is not number...");
    }

    public IndexIncorrectException() {
        super("Error! This value is incorrect...");
    }

}
