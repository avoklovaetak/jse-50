package ru.volkova.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.EndpointLocator;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.marker.IntegrationCategory;

import java.util.ArrayList;
import java.util.List;

public class AdminUserEndpointTest {

    private final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    @Before
    @SneakyThrows
    public void before() {
        session = endpointLocator.getSessionEndpoint().openSession("admin", "pass");
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void addUserTest() {
        UserGraph user = new UserGraph();
        endpointLocator.getAdminUserEndpoint().addUser(session, user);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findUserByIdTest() {
        UserGraph user = new UserGraph();
        endpointLocator.getAdminUserEndpoint().addUser(session, user);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findUserByLoginTest() {
        UserGraph user = new UserGraph();
        user.setLogin("login");
        endpointLocator.getAdminUserEndpoint().addUser(session, user);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint()
                .findUserByLogin(session, user.login));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void lockUserByEmailTest() {
        UserGraph user = new UserGraph();
        user.setEmail("testtest@test.ru");
        endpointLocator.getAdminUserEndpoint().addUser(session, user);
        endpointLocator.getAdminUserEndpoint().lockByEmail(session, user.email);
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().removeUserByEmail(session, user.email);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void lockUserByIdTest() {
        UserGraph user = new UserGraph();
        endpointLocator.getAdminUserEndpoint().addUser(session, user);
        endpointLocator.getAdminUserEndpoint().lockById(session, user.id);
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().removeUserById(session, user.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void lockUserByLoginTest() {
        UserGraph user = new UserGraph();
        user.setLogin("qwerty");
        endpointLocator.getAdminUserEndpoint().addUser(session, user);
        endpointLocator.getAdminUserEndpoint().lockByLogin(session, user.login);
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint()
                .findUserByLogin(session, user.login).locked);
        endpointLocator.getAdminUserEndpoint().removeUserByLogin(session, user.login);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void unlockUserByEmailTest() {
        UserGraph user = new UserGraph();
        user.setEmail("testtest@test.ru");
        endpointLocator.getAdminUserEndpoint().addUser(session, user);
        endpointLocator.getAdminUserEndpoint().lockByEmail(session, user.email);
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().unlockUserByEmail(session, user.email);
        Assert.assertTrue(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().removeUserByEmail(session, user.email);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void unlockUserByIdTest() {
        UserGraph user = new UserGraph();
        endpointLocator.getAdminUserEndpoint().addUser(session, user);
        endpointLocator.getAdminUserEndpoint().lockById(session, user.id);
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().unlockUserById(session, user.id);
        Assert.assertTrue(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().removeUserById(session, user.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void unlockUserByLoginTest() {
        UserGraph user = new UserGraph();
        user.setLogin("qwerty");
        endpointLocator.getAdminUserEndpoint().addUser(session, user);
        endpointLocator.getAdminUserEndpoint().lockByLogin(session, user.login);
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint()
                .findUserByLogin(session, user.login).locked);
        endpointLocator.getAdminUserEndpoint().unlockUserByLogin(session, user.login);
        Assert.assertTrue(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id).locked);
        endpointLocator.getAdminUserEndpoint().removeUserByLogin(session, user.login);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeUserByLoginTest() {
        UserGraph user = new UserGraph();
        user.setLogin("login1");
        endpointLocator.getAdminUserEndpoint().addUser(session, user);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint()
                .findUserByLogin(session, user.login));
        endpointLocator.getAdminUserEndpoint().removeUserByLogin(session, user.login);
        Assert.assertNull(endpointLocator.getAdminUserEndpoint()
                .findUserByLogin(session, user.login));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeUserByIdTest() {
        UserGraph user = new UserGraph();
        endpointLocator.getAdminUserEndpoint().addUser(session, user);
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint()
                .findUserById(session, user.id));
        endpointLocator.getAdminUserEndpoint().removeUserById(session, user.id);
        Assert.assertNull(endpointLocator.getAdminUserEndpoint()
                .findUserByLogin(session, user.id));
    }

}
