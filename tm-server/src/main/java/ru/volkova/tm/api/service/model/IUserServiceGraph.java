package ru.volkova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.model.UserGraph;

public interface IUserServiceGraph extends IServiceGraph<UserGraph> {

    void setPassword(@NotNull String userId, @Nullable String password);

}
