package ru.volkova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.model.IUserServiceGraph;
import ru.volkova.tm.model.UserGraph;
import ru.volkova.tm.repository.model.UserRepositoryGraph;

import javax.persistence.EntityManager;

public final class UserServiceGraph extends AbstractServiceGraph<UserGraph> implements IUserServiceGraph {

    public UserServiceGraph(
            @NotNull final IConnectionService connectionService
    ) {
        super(connectionService);
    }

    public void setPassword(
            @NotNull final String userId,
            @Nullable final String password
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepositoryGraph userRepository = new UserRepositoryGraph(entityManager) ;
            userRepository.setPassword(userId, password);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
