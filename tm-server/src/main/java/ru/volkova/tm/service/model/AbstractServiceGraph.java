package ru.volkova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.model.IServiceGraph;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.model.AbstractEntityGraph;

public abstract class AbstractServiceGraph<E extends AbstractEntityGraph> implements IServiceGraph<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractServiceGraph(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
