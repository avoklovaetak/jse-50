package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.api.service.dto.IProjectService;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.dto.Project;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.service.dto.ProjectService;

import static ru.volkova.tm.enumerated.Status.*;

public class ProjectGraphServiceTest {

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    private final IPropertyService propertyService = serviceLocator.getPropertyService();

    private final IConnectionService connectionService = serviceLocator.getConnectionService();

    private final IProjectService projectService = serviceLocator.getProjectDTOService();

    private final User user  = new User();

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        projectService.add(user.getId(), "DEMO", "project");
        final Project project = projectService.findOneByName(user.getId(), "DEMO");
        Assert.assertNotNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void changeOneStatusByIdTest() {
        final Project project = new Project();
        project.setUser(user);
        projectService.insert(project);
        projectService.changeOneStatusById(user.getId(), project.getId(), NOT_STARTED);
        Assert.assertEquals(NOT_STARTED, project.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeOneStatusByNameTest() {
        final Project project = new Project();
        project.setUser(user);
        project.setName("test");
        projectService.insert(project);
        projectService.changeOneStatusByName(user.getId(), project.getName(), IN_PROGRESS);
        Assert.assertEquals(IN_PROGRESS, project.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        final Project project = new Project();
        project.setUser(user);
        projectService.insert(project);
        projectService.clear(project.getUser().getId());
        Assert.assertTrue(projectService.findAll(user.getId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        final Project project = new Project();
        project.setUser(user);
        projectService.insert(project);
        Assert.assertNotNull(projectService.findById(project.getUser().getId(), project.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        final Project project = new Project();
        project.setUser(user);
        projectService.insert(project);
        Assert.assertNotNull(projectService.findOneByIndex(project.getUser().getId(), 0));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByNameTest() {
        final Project project = new Project();
        project.setUser(user);
        project.setName("DEMO");
        projectService.insert(project);
        Assert.assertNotNull(projectService.findOneByName(project.getUser().getId(), project.getName()));
    }

}
