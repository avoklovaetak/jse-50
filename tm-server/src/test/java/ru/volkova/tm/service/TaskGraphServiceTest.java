package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.api.service.dto.ITaskService;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.dto.Task;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.service.dto.TaskService;

import java.util.ArrayList;
import java.util.List;

public class TaskGraphServiceTest {

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    private final IPropertyService propertyService = serviceLocator.getPropertyService();

    private final IConnectionService connectionService = serviceLocator.getConnectionService();

    @NotNull
    private final ITaskService taskService = serviceLocator.getTaskDTOService();

    @NotNull
    private final User user = new User();

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final Task task = new Task();
        Assert.assertNotNull(taskService.insert(task));
    }

    @Test
    @Category(UnitCategory.class)
    public void addWithParametersTest() {
        taskService.add("1", "Project 1", "it is project");
        final Task task = taskService.findOneByName(user.getId(), "Project 1");
        Assert.assertNotNull(task);
        Assert.assertEquals("1", task.getUser());
        Assert.assertEquals("Project 1", task.getName());
        Assert.assertEquals("it is project", task.getDescription());
    }

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<Task> taskList = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        task1.setUser(user);
        task2.setUser(user);
        taskList.add(task1);
        taskList.add(task2);
        taskService.addAll(taskList);
        Assert.assertEquals(
                taskService.findById(task1.getUser().getId(),task1.getId()),
                task1);
        Assert.assertEquals(
                taskService.findById(task2.getUser().getId(),task2.getId()),
                task2);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final Task task = new Task();
        task.setUser(user);
        taskService.insert(task);
        Assert.assertNotNull(taskService.findAll(task.getUser().getId()));
        taskService.removeById(user.getId(), task.getId());
        Assert.assertTrue(taskService.findAll(task.getUser().getId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        final Task task = new Task();
        task.setUser(user);
        taskService.insert(task);
        Assert.assertNotNull(taskService.findAll(task.getUser().getId()));
        taskService.clear(user.getId());
        Assert.assertTrue(taskService.findAll(user.getId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByIdTest() {
        final Task task = new Task();
        task.setUser(user);
        taskService.insert(task);
        Assert.assertEquals(
                taskService.findById(task.getUser().getId(), task.getId()),
                task);
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByIndexTest() {
        final Task task = new Task();
        task.setUser(user);
        taskService.insert(task);
        Assert.assertEquals(
                taskService.findOneByIndex(task.getUser().getId(), 0),
                task);
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByNameTest() {
        final Task task = new Task();
        task.setUser(user);
        task.setName("DEMO");
        taskService.insert(task);
        Assert.assertEquals(
                taskService.findOneByName(task.getUser().getId(), task.getName()),
                task);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIdTest() {
        final Task task = new Task();
        task.setUser(user);
        taskService.insert(task);
        Assert.assertNotNull(taskService.findById(task.getUser().getId(), task.getId()));
        taskService.removeById(task.getUser().getId(), task.getId());
        Assert.assertTrue(taskService.findAll(task.getUser().getId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByNameTest() {
        final Task task = new Task();
        task.setUser(user);
        task.setName("DEMO");
        taskService.insert(task);
        Assert.assertEquals(
                taskService.findOneByName(task.getUser().getId(), task.getName()),
                task);
        taskService.removeOneByName(task.getUser().getId(), task.getName());
        Assert.assertTrue(taskService.findAll(task.getUser().getId()).isEmpty());
    }

}
