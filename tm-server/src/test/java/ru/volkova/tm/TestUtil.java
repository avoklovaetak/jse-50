package ru.volkova.tm;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.dto.IAdminUserService;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.service.ConnectionService;
import ru.volkova.tm.service.PropertyService;
import ru.volkova.tm.service.dto.AdminUserService;

public class TestUtil {

    @NotNull
    static final IPropertyService propertyService = new PropertyService();

    @NotNull
    static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    static final IAdminUserService userService = new AdminUserService(connectionService, propertyService);

    public static void initUser() {
        if (userService.findByLogin("test") != null) {
            userService.createUserWithEmail("test", "test", "test@test.ru");
        }
        if (userService.findByLogin("test2") != null) {
            userService.createUserWithEmail("test2", "test", "test@test.ru");
        }
        if (userService.findByLogin("admin") != null) {
            userService.createUserWithRole("admin", "admin", Role.ADMIN);
        }
    }


}
